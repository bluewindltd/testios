﻿using UnityEngine;
using System.Collections;

public class TestIOS2 : MonoBehaviour {
	int time = 0;
	
	// Use this for initialization
	void Start () {
		StartCoroutine (TestTime ());
	}
	
	IEnumerator TestTime()
	{
		Debug.Log ("TestTime TestIOS2 : " + time);
		time++;
		
		yield return new WaitForSeconds(1.0f);
		
		if (time >= 5) {
			Application.LoadLevel(0);
		} else {
			StartCoroutine (TestTime ());
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
